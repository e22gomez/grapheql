lien gitlab : https://gitlab.imt-atlantique.fr/e22gomez/grapheql.git


# UE-AD-A1-GRAPHQL
**Esteban GOMEZ et Capucine BECHEMIN**

_Ce TP modelise une gestion de séances de cinéma._

TP VERT : terminé

### Mise en place
* Clonez le repository
* Ouvrez ce dossier dans votre IDE
* Pour chacun des services ouvrez un terminal à la racine du TP 
* Entrez les commandes suivantes dans chacun d'eux

`cd .\movie\ ` `py .\movie.py`


`cd .\booking\ ` `py .\booking.py`


`cd .\showtime\ ` `py .\showtime.py`


`cd .\user\ ` `py .\user.py`


Pour lancer des requetes nous allons sur la route http://localhost:3001/graphql .

Des lignes de tests exemples sont presentes au bas des fonctions du fichier resolvers.py



### Documentation des routes des APIs

Nous avons implémenté ces routes dans le TP Rest en voici le lien : 
`https://gitlab.imt-atlantique.fr/c22beche/tp_rest.git`


* Movie
`http://localhost:3200/help`
* Booking
`http://localhost:3201/help`
* Showtimes
`http://localhost:3202/help`
* User
`http://localhost:3203/help`
