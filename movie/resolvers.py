import json

# All these functions contain the business codes

# Get the movie by his ID
def movie_with_id(_, info, _id):
    with open('{}/data/movies.json'.format("."), "r") as file:
        movies = json.load(file)
        for movie in movies['movies']:
            if movie['id'] == _id:
                return movie

# query{ movie_with_id(_id:"720d006c-3a57-4b6a-b18f-9b713b073f3c"){title}}


# Get the movie by his title
def movie_with_title(_, info, _title):
    with open('{}/data/movies.json'.format("."), "r") as file:
        movies = json.load(file)
        for movie in movies['movies']:
            if movie['title'] == _title:
                return movie
# query{movie_with_title(_title:"The Good Dinosaur"){id}}


# Get all the movie with the given rate
def movies_with_rate(_, info, _rating):
    with open('{}/data/movies.json'.format("."), "r") as file:
        movies = json.load(file)
        res = []
        for movie in movies['movies']:
            if movie['rating'] == _rating:
                res.append(movie)
        return res
# query{movies_with_rate(_rating:7.4){title}}


# Get all the movies
def movies(_, info):
    with open('{}/data/movies.json'.format("."), "r") as file:
        movies = json.load(file)
        return movies["movies"]
# query{movies{id}}


# Update an existing movie
def update_movie_rate(_,info,_id,_rate):
    newmovies = {}
    newmovie = {}
    with open('{}/data/movies.json'.format("."), "r") as rfile:
        movies = json.load(rfile)
        for movie in movies['movies']:
            if movie['id'] == _id:
                movie['rating'] = _rate
                newmovie = movie
                newmovies = movies
    with open('{}/data/movies.json'.format("."), "w") as wfile:
        json.dump(newmovies, wfile)
    return newmovie
# mutation {update_movie_rate(_id:"720d006c-3a57-4b6a-b18f-9b713b073f3c", _rate:3.0) {id}}

# Add a new Movie
def add_movie(_,info, _movie):
    with open('{}/data/movies.json'.format("."), "r") as rfile:
        movies = json.load(rfile)
        for movie in movies['movies']:
            if movie['id'] == _movie["id"]:
                return movie
    movies['movies'].append(_movie)
    with open('{}/data/movies.json'.format("."), "w") as wfile:
        json.dump(movies, wfile)
    return _movie
# test line :
# mutation{add_movie(_movie: {title: "The Good Test",rating: 9.0,director: "Capupu Esteban", id: "idtropcool"}){id}}


# Remove an existing movie
def remove_movie(_,info, _id):
    newmovies = {}
    removedmovie = {}
    with open('{}/data/movies.json'.format("."), "r") as rfile:
        movies = json.load(rfile)
        for movie in movies['movies']:
            if movie['id'] == _id:
                removedmovie = movie
                movies['movies'].remove(movie)
                newmovies = movies
    with open('{}/data/movies.json'.format("."), "w") as wfile:
        json.dump(movies, wfile)
    return removedmovie
# mutation {remove_movie(_id:"720d006c-3a57-4b6a-b18f-9b713b073f3c"){title}}


# Allow us to get the actors when we get a movie
def resolve_actors_in_movie(movie, info):
    with open('{}/data/actors.json'.format("."), "r") as file:
        data = json.load(file)
        actors = [actor for actor in data['actors'] if movie['id'] in actor['films']]
        return actors

